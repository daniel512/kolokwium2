﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium2
{
    class Klient
    {
        public delegate void NoweZgloszenieEvent();
        public event NoweZgloszenieEvent NoweZgloszenie;

        protected virtual void OnNoweZgloszenie(object o, EventArgs e)
        {
            NoweZgloszenie();
        }
            
      
        public void OtrzymanoZgloszenie()
        {
            Console.WriteLine("Klient: Nowe Zgłosznie");
            OnNoweZgloszenie(this, EventArgs.Empty);
        }
    }
}
