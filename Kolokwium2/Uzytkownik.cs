﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium2
{
    class Uzytkownik
    {
        private string _login;
        private string _haslo;
        private DateTime _dataUtworzenia;
        private int _iloscPostow;

        public Uzytkownik(string login, string haslo, int ilosc)
        {
            var rnd = new Random();
            this._login = login;
            this._haslo = haslo;
            this._iloscPostow = ilosc;
            this._dataUtworzenia = new DateTime(2019, 01, 01)
                                  .AddDays(rnd.Next(5, 500))
                                  .AddMinutes(rnd.Next(5, 1750));
        }

        public (string, DateTime, int) GetDane()
        {
            return (this._login, this._dataUtworzenia, this._iloscPostow);
        }

        public (string, int) GetLoginAndIloscPostow()
        {
            return (this._login, this._iloscPostow);
        }
    }
}
