﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kolokwium2
{
    public static class ExtensionMethods
    {
        public static string HashName(this string name)
        {
            string hashedName = "";

            for(int i = 0; i < name.Length; i++)
            {
                if(i < 1)
                {
                    hashedName += name[i];
                } else
                {
                    hashedName += '*';
                }
            }

            return hashedName;
        }
    }
}
