﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kolokwium2
{
    class Program
    {

        #region funkcje_z1

        public static IEnumerable<Post> PostyChronologicznie(List<Post> posty)
        {
            IEnumerable<Post> filtrowanePosty =
                from post in posty
                orderby post.DataUtworzenia
                select post;

            return filtrowanePosty;
        }

        public static IEnumerable<Post> PostyPoTytule(List<Post> posty)
        {
            IEnumerable<Post> filtrowanePosty =
              from post in posty
              orderby post.Tytul
              select post;

            return filtrowanePosty;
        }

        public static IEnumerable<TSource> Page<TSource>(IEnumerable<TSource> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }
        #endregion

        #region funkcja_z5
        static string Sklej<T>(T obiekt1, T obiekt2)
        {
            return obiekt1.ToString() + obiekt2.ToString();
        }
        #endregion

        static void Main(string[] args)
        {
            #region z1

            List<Post> posty = new List<Post>();

            for(int i = 0; i < 100; i++)
            {
                posty.Add(new Post("Tytul", "Autor", "Tresc"));
            }

            var filtrowanePosty = PostyChronologicznie(posty);
            var filtrowanePosty2 = Page(filtrowanePosty, 1, 10);

            foreach (var post in filtrowanePosty)
            {
                Console.WriteLine($"{post.Tytul}  | { post.Tresc} | {post.Autor} | {post.DataUtworzenia}");
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            foreach (var post in filtrowanePosty2)
            {
                Console.WriteLine($"{post.Tytul}  | { post.Tresc} | {post.Autor} | {post.DataUtworzenia}");
            }
            #endregion
            #region z2
            string nazwisko = "Kowalski";
            Console.WriteLine(nazwisko.HashName());
            #endregion
            #region z3
            Uzytkownik u1 = new Uzytkownik("Jan", "Haslo1234", 12);
            var(login, data, iloscPostow) = u1.GetDane();
            var (login2, iloscPostow2) = u1.GetLoginAndIloscPostow();

            Console.WriteLine(login + "     " + iloscPostow2);
            #endregion

            Klient k1 = new Klient();
            Bank b1 = new Bank();
            k1.NoweZgloszenie += b1.OtrzymanoZgloszenie;

            k1.OtrzymanoZgloszenie();

            #region z5
            Uzytkownik u5 = new Uzytkownik("Jan", "Haslo1234", 12);
            Console.WriteLine(Sklej<Uzytkownik>(u1, u5));
            Console.WriteLine(Sklej<string>("abcde", "fghijk"));
            #endregion
        }
    }
}
