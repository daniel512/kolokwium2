﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kolokwium2
{
    class Post
    {
        public DateTime DataUtworzenia;
        public string Tytul;
        public string Autor;
        public string Tresc;

        public Post(string tytul, string autor, string tresc)
        {
            var rnd = new Random();
            this.Tytul = tytul;
            this.Autor = autor;
            this.Tresc = tresc;
            this.DataUtworzenia = new DateTime(2019, 01, 01)
                                    .AddDays(rnd.Next(5, 500))
                                    .AddMinutes(rnd.Next(5, 1750));
        }

    
    }
}
